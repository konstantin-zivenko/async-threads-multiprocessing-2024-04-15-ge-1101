import time
import asyncio

from numbers import Number


async def func_1(x_in: Number) -> str:
    print("func_1 started")
    await asyncio.sleep(2)
    return f"func_1({x_in}): {pow(x_in, 2)}"


async def func_2(x_in: Number) -> str:
    print("func_2 started")
    await asyncio.sleep(2)
    return f"func_2({x_in}): {pow(x_in, 0.5)}"


# async def main(x_in: Number) -> None:
#     print("Before create task ...")
#     task_1 = asyncio.create_task(func_1(x_in))
#     task_2 = asyncio.create_task(func_2(x_in))
#     print("After create task ...")
#     # res_1 = await func_1(x_in)
#     res_1 = await task_1
#     print(res_1)
#     # res_2 = await func_2(x_in)
#     res_2 = await task_2
#     print(res_2)

async def main(x_in):
    async with asyncio.TaskGroup() as tg:
        res_1 = tg.create_task(func_1(x_in))
        res_2 = tg.create_task(func_2(x_in))
        # res_1, res_2 = (tg.create_task(func_1(x_in)), tg.create_task(func_2(x_in)))
    print(res_1.result())
    print(res_2.result())


if __name__ == "__main__":
    x = 9
    start = time.perf_counter()
    asyncio.run(main(x))
    duration = time.perf_counter() - start
    print(duration)