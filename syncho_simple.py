import time
from numbers import Number


def func_1(x_in: Number) -> str:
    print("func_1 started")
    time.sleep(2)
    return f"func_1({x_in}): {pow(x_in, 2)}"


def func_2(x_in: Number) -> str:
    print("func_2 started")
    time.sleep(2)
    return f"func_2({x_in}): {pow(x_in, 0.5)}"


def main(x_in: Number) -> None:
    res_1 = func_1(x_in)
    print(res_1)
    res_2 = func_2(x_in)
    print(res_2)


if __name__ == "__main__":
    x = 9
    start = time.perf_counter()
    main(x)
    duration = time.perf_counter() - start
    print(f"Total code execution time: {duration}")
