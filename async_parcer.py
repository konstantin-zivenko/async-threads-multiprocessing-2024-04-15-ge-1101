import asyncio

import httpx
import time
from bs4 import BeautifulSoup

URL = "https://djinni.co/jobs/"                             # All


async def get_djinni_jobs(page: int, client: httpx.AsyncClient, url: str = URL):
    response = await client.get(url, params={"page": page})
    soup = BeautifulSoup(response.content, "html.parser")
    return [
        job.text.strip() for job in soup.findAll("a", class_="h3 job-list-item__link")
    ]


async def main(start_page_in: int, stop_page_in: int) -> None:
    async with httpx.AsyncClient() as client:
        async with asyncio.TaskGroup() as tg:
            tasks = [
                tg.create_task(get_djinni_jobs(page, client, URL))
                for page in range(start_page_in, stop_page_in)
            ]

    for task in tasks:
        print(task.result())


if __name__ == "__main__":
    start_page = 10
    stop_page = 160
    start = time.perf_counter()
    asyncio.run(main(start_page, stop_page))
    duration = time.perf_counter() - start
    print(f"Duration: {duration}")
